<?php

use yii\db\Migration;

/**
 * Class m210219_155729_blog_db
 */
class m210219_155729_blog_db extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
       $this->execute("
       
       CREATE TABLE `author` 
       ( 
           `id` SERIAL NOT NULL , 
           `name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL , 
           `lastname` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL , 
           PRIMARY KEY (`id`), 
           INDEX (`name`), 
           INDEX (`lastname`)
       ) ENGINE = InnoDB;
       
       CREATE TABLE `post` 
       ( 
           `id` SERIAL NOT NULL , 
           `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL , 
           `id_author` INT NOT NULL , 
           `text` LONGTEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL , 
           `created_at` DATE NOT NULL , 
           `image_url` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_bin NULL , 
           PRIMARY KEY (`id`), 
           INDEX (`title`), 
           INDEX (`id_author`), 
           INDEX (`created_at`)
       ) ENGINE = InnoDB;

       CREATE TABLE `comment` 
       ( 
           `id` SERIAL NOT NULL , 
           `id_post` INT NOT NULL , 
           `text` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL , 
           PRIMARY KEY (`id`), 
           INDEX (`id_post`)
       ) ENGINE = InnoDB;
       
       ");
    }

    public function down()
    {
        $this->execute("
            drop table IF EXISTS `comment`;
            drop table IF EXISTS `post`;
            drop table IF EXISTS `author`;
       ");
    }
}
