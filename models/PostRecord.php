<?php

namespace app\models;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property int $id_author
 * @property string $text
 * @property string $created_at
 * @property string|null $image_url
 *
 * @property AuthorRecord $author
 * @property CommentRecord [] $comments
 */
class PostRecord extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'id_author', 'text'], 'required'],
            [['id_author'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['title', 'image_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'id_author' => 'Id Author',
            'text' => 'Text',
            'created_at' => 'Publication Date',
            'image_url' => 'Image',
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(AuthorRecord::class, ['id' => 'id_author']);
    }

    public function getComments()
    {
        return  $this->hasMany(CommentRecord::class, ['id_post' => 'id']);
    }

    public function upload($file)
    {
        if(!is_null($file)){
            $baseName = md5($file->baseName . $this->id);
            $file->saveAs('uploads/' . $baseName . '.' . $file->extension);
            $this->image_url = 'uploads/' . $baseName . '.' . $file->extension;
            $this->save();
            return true;
        }
        return false;
    }
}
