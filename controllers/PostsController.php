<?php

namespace app\controllers;

use app\models\CommentRecord;
use app\services\ExportService;
use Yii;
use app\models\PostRecord;
use app\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PostsController implements the CRUD actions for PostRecord model.
 */
class PostsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PostRecord models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PostRecord model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $postModel = $this->findModel($id);
        $comments = $postModel->comments;
        return $this->render('view', [
            'model' => $postModel,
            'comments' => $comments
        ]);
    }

    /**
     * Creates a new PostRecord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PostRecord();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $file = UploadedFile::getInstance($model, 'image_url');
            $model->upload($file);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PostRecord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $file = UploadedFile::getInstance($model, 'image_url');
            if(!$model->upload($file)){
               $oldImage = Yii::$app->request->post('oldImage', '');
               $model->image_url = $oldImage;
               $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PostRecord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $post = $this->findModel($id);
        if(!empty($post->image_url) && !is_null($post->image_url)){
            $path = Yii::getAlias("@webroot");
            if(file_exists($path . '/' . $post->image_url)){
                unlink($path . '/' . $post->image_url);
            }
        }
        $post->delete();


        return $this->redirect(['index']);
    }

    /**
     * Finds the PostRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PostRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PostRecord::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionNewComment()
    {
        if(!Yii::$app->request->isAjax){
            return $this->goHome();
        }
        $postId = Yii::$app->request->post('postId');
        $post = $this->findModel($postId);
        $comment = new CommentRecord();
        $comment->load(Yii::$app->request->post());
        if(!empty($comment->text)){
            $comment->save();
        }
        return $this->renderAjax('_comments', ['comments' => $post->comments]);
    }

    public function actionExportToCsv()
    {
        $es = new ExportService();
        $data = $es->generateCsv();
        return Yii::$app->response->sendContentAsFile($data, 'export_' . date('d.m.Y') . '.csv', [
            'mimeType' => 'text/csv',
            'inline'   => false
        ]);
    }

    public function actionDropImage()
    {
        if(!Yii::$app->request->isAjax){
            return $this->goHome();
        }
        $postId = Yii::$app->request->post('postId');
        $post = $this->findModel($postId);
        if(!empty($post->image_url) && !is_null($post->image_url)){
            $path = Yii::getAlias("@webroot");
            if(file_exists($path . '/' . $post->image_url)){
                unlink($path . '/' . $post->image_url);
                $post->image_url = '';
                $post->save();
            }
        }
    }
}
