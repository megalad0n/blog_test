<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AuthorRecord */

$this->title = 'Create Author Record';
$this->params['breadcrumbs'][] = ['label' => 'Author Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-record-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
