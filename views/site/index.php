<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <H4><?=Html::a('Posts', ['posts/index']);?></H4>
    <H4><?=Html::a('Authors', ['authors/index']);?></H4>
</div>
