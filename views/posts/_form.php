<?php

use app\models\AuthorRecord;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostRecord */
/* @var $form yii\widgets\ActiveForm */

$authors = [];
$raw = AuthorRecord::find()->orderBy('id')->asArray()->all();
foreach ($raw as $item){
    $authors[] = [
        'id' => $item['id'],
        'name' => $item['name'] . ' ' . $item['lastname'],
    ];
}

$js = "
 function dropFile()
   {
       var data = [];  
       data.push({name: 'postId', value : '".$model->id."'});
       $.ajax({
            url: '".Url::to(['posts/drop-image'])."',
            type: 'POST',
            data: data,
            success: function(res){
              $('#oldImage').val('');
              $('#image-div').hide(); 
            },
            error: function(){
                alert('Error!');
            }
        });
   }
";

$this->registerJs($js, View::POS_HEAD);

?>

<div class="post-record-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_author')->widget(Select2::class,[
        'data' => ArrayHelper::map($authors, 'id', 'name'),
        'options' => ['placeholder' => 'Choose author'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Author') ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->widget(DatePicker::class,[
        'type' => DatePicker::TYPE_INPUT,
        'value' => !is_null($model->created_at)? $model->created_at : '',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <input type="hidden" name="oldImage" value="<?=$model->image_url?>" id="oldImage">

    <?= $form->field($model, 'image_url')->fileInput() ?>
    <?php
    if(!is_null($model->image_url) && !empty($model->image_url)){
        echo '<div id="image-div" style="margin-bottom: 20px;">';
        echo  '<img id="image" src="'. Yii::getAlias('@web') . '/' . $model->image_url . '" height="150px" alt="image not found">';
        echo  "<a href=\"#\" onclick=\"dropFile();\"><i class='glyphicon glyphicon-remove'></i></a>";
        echo '</div>';
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
