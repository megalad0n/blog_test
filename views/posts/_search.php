<?php

use app\models\AuthorRecord;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostSearch */
/* @var $form yii\widgets\ActiveForm */

$authors = [];
$raw = AuthorRecord::find()->orderBy('id')->asArray()->all();
foreach ($raw as $item){
    $authors[] = [
        'id' => $item['id'],
        'name' => $item['name'] . ' ' . $item['lastname'],
    ];
}
?>

<div class="post-record-search" style="border: #0f0f0f 1px solid; margin: 10px; padding: 5px; border-radius: 10px;">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'id_author')->widget(Select2::class,[
        'data' => ArrayHelper::map($authors, 'id', 'name'),
        'options' => ['placeholder' => 'Choose author'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'text') ?>

    <?= $form->field($model, 'created_at')->widget(DatePicker::class,[
        'type' => DatePicker::TYPE_INPUT,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
