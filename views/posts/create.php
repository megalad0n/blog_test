<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PostRecord */

$this->title = 'Create Post Record';
$this->params['breadcrumbs'][] = ['label' => 'Post Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-record-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
