<?php

/* @var app\models\CommentRecord $model */
/* @var integer $postId */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div style="border: #1b6d85 1px solid; margin-bottom: 20px; padding: 10px;">
<?php
$form = ActiveForm::begin(['id' => 'commentForm']); ?>

    <?= $form->field($model, 'id_post')->hiddenInput(['value' => $postId])->label(false) ?>
    <?= $form->field($model, 'text')->textInput(['id' => 'commentField', 'placeholder' => 'Input comment'])->label(false) ?>

    <div class="form-group">
        <?=Html::button('Send', ['id' => 'sendButton', 'onclick' => 'sendComment();'])?>
    </div>

<?php ActiveForm::end(); ?>

</div>
