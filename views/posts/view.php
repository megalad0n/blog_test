<?php

use app\models\CommentRecord;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PostRecord */
/* @var $comments [] app\models\CommentRecord */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Post Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);

$js = "
 function sendComment()
   {
       var data = $('#commentForm :input').serializeArray();  
       data.push({name: 'postId', value : '".$model->id."'});
       $.ajax({
            url: '".Url::to(['posts/new-comment'])."',
            type: 'POST',
            data: data,
            success: function(res){
                $('#post_comments').html(res);    
                $('#commentField').val('');            
            },
            error: function(){
                alert('Error!');
            }
        });
   }
";

$this->registerJs($js, View::POS_HEAD);
?>
<div class="post-record-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
               'format' => 'raw',
               'label' => 'Author',
               'value' => function($model){
                   return $model->author->name . ' ' . $model->author->lastname;
               }
            ],
            'text:ntext',
            'created_at',
            [
              'attribute' => 'image_url',
              'format' => 'raw',
              'value' => function($model){
                   return (is_null($model->image_url) || empty($model->image_url))? '' : '<img src="'.Yii::getAlias('@web') . '/' . $model->image_url.'">';
              }
            ],
        ],
    ]) ?>

    <?=$this->render('_commentform', ['model' => (new CommentRecord()), 'postId' => $model->id])?>
    <div id="post_comments">
       <?=$this->render('_comments', ['comments' => $comments]);?>
    </div>

</div>
