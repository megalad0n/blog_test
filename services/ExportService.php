<?php

namespace app\services;

use app\models\PostRecord;

class ExportService
{
   public function generateCsv()
   {
       $titles = ['Title', 'Author', 'Text', 'Created_ad'];
       $query = "select p.title, concat(a.name, ' ', a.lastname) as author, p.text, p.created_at
                 from post p inner join author a on a.id = p.id_author";
       $result = implode(";", $titles) . "\r\n";
       $data = \Yii::$app->db->createCommand($query)->queryAll();
       foreach ($data as $item) {
           $result .= $item['title']. ';'
                    . $item['author']. ';'
                    . $item['text']. ';'
                    . $item['created_at'].
               "\r\n";
       }

       return $result;
   }
}